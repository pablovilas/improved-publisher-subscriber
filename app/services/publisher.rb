class Publisher < Connection

  def initialize(topic)
    @topic = topic.to_s
  end

  def publish(action, message)
    exchange = channel.topic(@topic)
    exchange.publish(message.to_json, routing_key: action)
  end

end
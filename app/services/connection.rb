require 'bunny'

class Connection

  def channel
    @channel ||= connection.create_channel
  end

  def connection
    if @connection.nil?
      at_exit do
        unless connection.nil?
          puts 'Closing RabbitMQ connection'
          @connection.close
          puts 'RabbitMQ connection closed'
        end
      end
      @connection = Bunny.new(
          host: 'localhost',
          port: 5672,
          ssl: false,
          vhost: '/',
          user: 'guest',
          pass: 'guest',
          heartbeat: :server,
          frame_max: 131072,
          auth_mechanism: "PLAIN"
      )
      @connection.start
    end
    @connection
  end

end
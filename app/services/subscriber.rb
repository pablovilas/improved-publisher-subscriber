class Subscriber < Connection

  def initialize(topic, queue)
    @topic = topic.to_s
    @queue = queue.to_s
  end

  def receive(actions = [])
    exchange = channel.topic(@topic)
    queue = channel.queue(@queue, durable: true)

    actions.each do |action|
      queue.bind(exchange, routing_key: action.to_s)
    end

    queue.subscribe(block: true) do |delivery_info, _properties, body|
      yield delivery_info.routing_key.to_s, JSON.parse(body, object_class: OpenStruct)
    end
  end

end